import { createSlice } from '@reduxjs/toolkit';

export const searcherSlice = createSlice({
  name: 'searcher',
  initialState: {
    value: '',
    coords: {
      lon: 0,
      lat: 0
    }
  },
  reducers: {
    changeValue: (state, action) => {
      state.value = action.payload;
    },
    changeCoords: (state, action) => {
      state.coords.lon = action.payload.coords.longitude;
      state.coords.lat = action.payload.coords.latitude;
    },
  },
});

export const { changeValue, changeCoords } = searcherSlice.actions;

export const careCoords = coords => dispatch => {
  if (coords.lon === 0 && coords.lat === 0) {
    navigator.geolocation.getCurrentPosition(res => dispatch(changeCoords(res)));
  }
}

export const selectValue = state => state.searcher.value;

export const selectCoords = state => state.searcher.coords;

export default searcherSlice.reducer;
