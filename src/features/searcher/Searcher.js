import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  selectValue,
  changeValue,
  selectCoords,
  careCoords
} from './searcherSlice';
import {
  getStyles
} from '../lightdark/Lightdark';
import Autocomplete from 'react-autocomplete';
const cities = require('../../data/city.list.json');

function shouldRender(city, value) {
  return value.length >= 3 && city.name.toLowerCase().indexOf(value.toLowerCase()) !== -1;
}

function shrinkMenu(items, value) {
  const maxLen = 10;
  let ret = [];
  for (let i = 0; i < items.length && i < maxLen; i++)
    ret.push(items[i]);
  ret.length = Math.min(items.length, maxLen);
  return (
    <div className="menu">
      {ret}
    </div>
  );
}

function calcCrow(lat1, lon1, lat2, lon2)  {
  var R = 6371;
  var dLat = toRad(lat2-lat1);
  var dLon = toRad(lon2-lon1);
  lat1 = toRad(lat1);
  lat2 = toRad(lat2);

  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c;
  return d;
}

function toRad(Value)  {
    return Value * Math.PI / 180;
}

function cutDigits(val) {
  return Math.round(val * 100) / 100;
}

function distance(a, b) {
  return calcCrow(a.lat, a.lon, b.lat, b.lon);
}

function sortCities(a, b, value) {
  const lenA = a.name.length;
  const lenB = b.name.length;
  if (lenA === lenB)
    return 0;
  return lenA < lenB ? -1 : 1;
}

function findMe(dispatch, coords) {
  let best = 0;
  for (let i = 0; i < cities.length; i++)
    if (distance(coords, cities[i].coord) < distance(coords, cities[best].coord))
      best = i;
  dispatch(changeValue(cities[best].name));
}

export function findBestId(coords, value) {
  let best = -1;
  for (let i = 0; i < cities.length; i++)
    if (cities[i].name === value && ( best === -1 || distance(coords, cities[i].coord) < distance(coords, cities[best].coord)))
      best = i;
  if (best === -1)
    return -1;
  return cities[best].id;
}

function ChosenCity() {
  const value = useSelector(selectValue);
  const coords = useSelector(selectCoords);
  const id = findBestId(coords, value);
  const styles = getStyles(useSelector);
  if (id === -1)
  {
    return (
      <div>
        Choose your city
      </div>
    );
  }
  return (
    <div>
      <div className={styles.row}>
        <span>Your city: {value}</span>
      </div>
    </div>
  );
}

export function Searcher() {
  const value = useSelector(selectValue);
  const coords = useSelector(selectCoords);
  const styles = getStyles(useSelector);
  const dispatch = useDispatch();
  dispatch(careCoords(coords));
  return (
    <div>
      <ChosenCity/>
      <div className={styles.row}>
        <button
          className={styles.button}
          aria-label="Find me"
          onClick={() => findMe(dispatch, coords)}
        >
          Find me
        </button>
      </div>
      <Autocomplete
        value={value}
        inputProps={{ id: 'cities' }}
        items={cities}
        shouldItemRender={shouldRender}
        getItemValue={item => item.name}
        onSelect={value => dispatch(changeValue(value)) }
        onChange={e => dispatch(changeValue(e.target.value)) }
        renderItem={(item, isHighlighted) => (
          <div
            style={{ background: isHighlighted ? 'lightgray' : 'white', color: 'black' }}
            className={`item ${isHighlighted ? 'item-highlighted' : ''}`}
            key={item.id}
          >
            {item.name+' ('+cutDigits(distance(item.coord, coords)).toString()+'km)'}
          </div>
        )}
        renderMenu={shrinkMenu}
        wrapperStyle={{ position: 'relative', display: 'inline-block' }}
        sortItems={sortCities}
      />
    </div>
  );
}
