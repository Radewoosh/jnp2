import { createSlice } from '@reduxjs/toolkit';

export const lightdarkSlice = createSlice({
  name: 'lightdark',
  initialState: {
    value: true,
  },
  reducers: {
    changeValue: state => {
      state.value = !state.value;
    },
  },
});

export const { changeValue } = lightdarkSlice.actions;

export const selectValue = state => state.lightdark.value;

export default lightdarkSlice.reducer;
