import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  changeValue,
  selectValue
} from './lightdarkSlice';
import lightStyles from '../../styles/Light.module.css';
import darkStyles from '../../styles/Dark.module.css';

export function getStyles(select) {
  if (select(selectValue))
    return lightStyles;
  else
    return darkStyles;
}

export function LightDark() {
  const dispatch = useDispatch();
  const styles = getStyles(useSelector);
  return (
    <div>
      <div className={styles.row}>
        <button
          className={styles.button}
          aria-label="Change lightdark"
          onClick={() => dispatch(changeValue())}
        >
          Change mode
        </button>
      </div>
    </div>
  );
}
