import { createSlice } from '@reduxjs/toolkit';

export const forecastSlice = createSlice({
  name: 'forecast',
  initialState: {
    id: -1,
    prediction: {},
    type: true
  },
  reducers: {
    changePrediction: (state, action) => {
      state.id = action.payload.id;
      state.prediction = action.payload.prediction;
    },
    changeType: (state, action) => {
      state.type = action.payload;
    },
  },
});

export const { changePrediction , changeType } = forecastSlice.actions;

async function sendRequest(req) {
  let response = await fetch(req);
  let ret = await response.json();
  return ret;
}

export const downloadData = (id, coords) => dispatch => {
  const pref = 'https://api.openweathermap.org/data/2.5/onecall?&lat=';
  const mid = '&lon='
  const suf = '&appid=b162620bd1be9eb3153e1e15e938aa65';
  const req = pref + coords.lat.toString() + mid + coords.lon.toString() + suf;
  sendRequest(req).then(data => dispatch(changePrediction({id: id, prediction: data})));
}

export const selectId = state => state.forecast.id;

export const selectPrediction = state => state.forecast.prediction;

export const selectType = state => state.forecast.type;

export default forecastSlice.reducer;
