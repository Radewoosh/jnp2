import React, { Component } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  selectId,
  selectPrediction,
  downloadData,
  selectType,
  changeType
} from './forecastSlice';
import {
  selectValue,
  selectCoords
} from '../searcher/searcherSlice';
import {
  findBestId
} from '../searcher/Searcher';
import {
  getStyles
} from '../lightdark/Lightdark';
const cities = require('../../data/city.list.json');

function getCityById(id) {
  for (let i = 0; i < cities.length; i++)
    if (cities[i].id === id)
      return cities[i];
  return {};
}

function cutDigits(val) {
  return Math.round(val * 100) / 100;
}

function howCool(table) {
  let noRainy = 1;
  let niceAverage = 1;
  let neverCritical = 1;
  let average = 0;
  for (let i = 0; i < table.length; i++) {
    if (table[i].desc === 'Rain')
      noRainy = 0;
    if (table[i].temp < 15 || table[i].temp > 30)
      neverCritical = 0;
    average += table[i].temp;
  }
  average /= table.length;
  if (average < 18 || average > 25)
    niceAverage = 0;
  return noRainy + niceAverage + neverCritical;
}

function howCoolString(table) {
  const take = howCool(table);
  if (take === 3)
    return "The weather will be nice";
  if (take === 2)
    return "The weather will be passable";
  return "The weather won't be nice";
}

function clearTable(type, table) {
  const Kelvin = 273.15;
  let ret = [];
  if (type) {
    for (let i = 0; i < 48; i++) {
      ret.push({when: i, temp: cutDigits(table[i].temp - Kelvin), pres: table[i].pressure, desc: table[i].weather[0].main});
    }
    ret.length = 48;
  } else {
    for (let i = 0; i < 7; i++) {
      ret.push({when: i, temp: cutDigits(table[i].temp.day - Kelvin), pres: table[i].pressure, desc: table[i].weather[0].main});
    }
    ret.length = 7;
  }
  return ret;
}

function addZero(num) {
  if (num >= 10)
    return num.toString();
  return '0'+num.toString();
}

class Table extends Component {
 constructor(props) {
    super(props)
    this.state = {
       forecast: props
    }
  }
  renderTableData() {
    const len = this.state.forecast.length;
    if (len === 48) {
      const d = new Date();
      return this.state.forecast.map((interval, index) => {
         const { when, temp, pres, desc } = interval
         return (
            <tr key={when}>
               <td>{(d.getHours() + when) % 24}:{addZero(d.getMinutes())}</td>
               <td>{temp}&#176;C</td>
               <td>{pres}hPa</td>
               <td>{desc}</td>
            </tr>
         )
      })
    } else {
      const d = new Date();
      const id = d.getDay();
      const str = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
      ]
      return this.state.forecast.map((interval, index) => {
         const { when, temp, pres, desc } = interval
         return (
            <tr key={when}>
               <td>{str[(id + when) % 7]} {d.getHours()}:{addZero(d.getMinutes())}</td>
               <td>{temp}&#176;C</td>
               <td>{pres}hPa</td>
               <td>{desc}</td>
            </tr>
         )
      })
    }
  }
  renderTableHeader() {
    let header = Object.keys(this.state.forecast[0])
    return header.map((key, index) => {
      if (key === "when")
        return <th key={index}>Time</th>
      if (key === "temp")
        return <th key={index}>Temperature</th>
      if (key === "pres")
        return <th key={index}>Pressure</th>
      if (key === "desc")
        return <th key={index}>Description</th>
      return <th key={index}>Error!</th>
    })
  }


  render() {
    const styles = getStyles(useSelector);
    return (
      <div className={styles.mytable}>
        <h2>{howCoolString(this.state.forecast)}</h2>
        <table id='forecast table'>
          <tbody>
            <tr>{this.renderTableHeader()}</tr>
            {this.renderTableData()}
          </tbody>
        </table>
      </div>
    )
  }
}

function PresentData(props) {
  const table = props.type ? props.data.hourly : props.data.daily;
  const niceTable = clearTable(props.type, table);
  console.log(table);
  let myTable = new Table(niceTable);
  myTable.state.forecast = niceTable;
  return myTable.render();
}

export function TypeChanger() {
  const dispatch = useDispatch();
  const wantedPrediction = useSelector(selectType);
  const styles = getStyles(useSelector);
  return (
    <div>
      <div className={styles.row}>
        Chosen Type: {wantedPrediction ? "hourly" : "daily"}
      </div>
      <div className={styles.row}>
        <button
          className={styles.button}
          aria-label="Change Type"
          onClick={() => dispatch(changeType(!wantedPrediction))}
        >
          Change type
        </button>
      </div>
    </div>
  );
}

export function Forecast() {
  const dispatch = useDispatch();
  const value = useSelector(selectValue);
  const coords = useSelector(selectCoords);
  const id = findBestId(coords, value);
  const cachedId = useSelector(selectId);
  const prediction = useSelector(selectPrediction);
  const wantedPrediction = useSelector(selectType);
  
  if (id !== cachedId && id !== -1)
    dispatch(downloadData(id, getCityById(id).coord));
  if (id === -1 || cachedId !== id)
    return (
      <div>
      </div>
    );
    
  return (
    <div>
      <PresentData
        data={prediction}
        type={wantedPrediction}
      />
    </div>
  );
}
