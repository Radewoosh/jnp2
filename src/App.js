import React from 'react';
import { useSelector } from 'react-redux';
import { Searcher } from './features/searcher/Searcher';
import { Forecast } from './features/forecast/Forecast';
import { TypeChanger } from './features/forecast/Forecast';
import { LightDark } from './features/lightdark/Lightdark';
import './App.css';
import {
  getStyles
} from './features/lightdark/Lightdark';

function App() {
  const styles = getStyles(useSelector);
  return (
    <div className="App">
      <div className={styles.app}>
        <div className={styles.sidebar_left}>
          <LightDark/>
          <TypeChanger/>
          <Searcher />
        </div>
        <header className="App-header">
          <Forecast />
        </header>
      </div>
    </div>
  );
}

export default App;
