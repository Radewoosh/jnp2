import { configureStore } from '@reduxjs/toolkit';
import searcherReducer from '../features/searcher/searcherSlice';
import forecastReducer from '../features/forecast/forecastSlice';
import lightdarkReducer from '../features/lightdark/lightdarkSlice';

export default configureStore({
  reducer: {
    searcher: searcherReducer,
    forecast: forecastReducer,
    lightdark: lightdarkReducer,
  },
});
